package com.yaylo.mornhouse.view.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yaylo.mornhouse.data.local.dao.NumberInfoDao
import com.yaylo.mornhouse.data.local.entity.NumberInfoEntity
import com.yaylo.mornhouse.data.network.model.NumberInfoModel
import com.yaylo.mornhouse.data.network.repositories.NumberInfoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SearchNumberFactsViewModel @Inject constructor(
    private val numberRepo: NumberInfoRepository,
    private val dao: NumberInfoDao
) :
    ViewModel() {
    val numberInfo: MutableLiveData<NumberInfoModel?> = MutableLiveData(null)
    val numberHistory: MutableLiveData<List<NumberInfoEntity>?> = MutableLiveData(null)

    fun getInfoForNumber(number: Long) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val response = numberRepo.getInfoForNumber(number)
                if (response.isSuccessful) {
                    numberInfo.postValue(response.body())
                    response.body()?.let {
                        saveInfo(it)
                    }
                }
            }
        }
    }

    fun getInfoForRandomNumber() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val response = numberRepo.getInfoForRandomNumber()
                if (response.isSuccessful) {
                    numberInfo.postValue(response.body())
                    response.body()?.let {
                        saveInfo(it)
                    }
                }
            }
        }
    }

    private fun saveInfo(info: NumberInfoModel) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                dao.insert(NumberInfoEntity.fromNetworkModel(info))
            }
        }
    }


    fun getHistoryFromDb() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                numberHistory.postValue(dao.getAll())
            }
        }
    }
}
