package com.yaylo.mornhouse.view.search

import android.app.AlertDialog
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import com.yaylo.mornhouse.R
import com.yaylo.mornhouse.data.local.entity.NumberInfoEntity
import com.yaylo.mornhouse.data.network.isOnline
import com.yaylo.mornhouse.databinding.FragmentSearchNumberFactsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchNumberFactsFragment : Fragment() {

    private var _binding: FragmentSearchNumberFactsBinding? = null
    private val binding get() = _binding!!

    private val adapter = HistoryAdapter(onItemClick = ::historyItemClick)
    private fun historyItemClick(item: NumberInfoEntity) = findNavController().navigate(
        R.id.action_searchNumberFactsFragment_to_numberInfoFragment,
        bundleOf(INFO_TITLE_TAG to item.title, INFO_TEXT_TAG to item.text)
    )

    private val viewModel: SearchNumberFactsViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchNumberFactsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        viewModel.getHistoryFromDb()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkInternetConnection()
        setupList()
        setupObservers()
        setListeners()

        binding.editTextNumber.transformationMethod = object : PasswordTransformationMethod() {
            override fun getTransformation(source: CharSequence?, view: View?): CharSequence {
                return source ?: ""
            }
        }
    }

    private fun setupList() {
        binding.historyList.adapter = adapter
        binding.historyList.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setListeners() {
        binding.findFactForInputtedNumberButton.setOnClickListener {
            if (checkInternetConnection()) {
                if (binding.editTextNumber.text.isNullOrBlank()) {
                    Toast.makeText(
                        requireActivity().applicationContext,
                        "Input some digits",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    viewModel.getInfoForNumber(binding.editTextNumber.text.toString().toLong())
                }
            }
        }
        binding.findRandomFactButton.setOnClickListener {
            if (checkInternetConnection()) {
                viewModel.getInfoForRandomNumber()
            }
        }
    }

    private fun setupObservers() {
        viewModel.numberInfo.observe(viewLifecycleOwner) {
            it?.let {
                if (it.found) {
                    findNavController().navigate(
                        R.id.action_searchNumberFactsFragment_to_numberInfoFragment,
                        bundleOf(INFO_TITLE_TAG to it.number.toString(), INFO_TEXT_TAG to it.text)
                    )
                } else {
                    findNavController().navigate(
                        R.id.action_searchNumberFactsFragment_to_numberInfoFragment
                    )
                }
            }
        }
        viewModel.numberHistory.observe(viewLifecycleOwner) { history ->
            history?.let {
                adapter.setItems(it)
            }
        }
    }

    private fun checkInternetConnection(): Boolean {
        if (!isOnline(requireContext())) {
            showNoInternetDialog()
            return false
        }
        return true
    }

    private fun showNoInternetDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("No Internet connection")
            .setMessage("Check your connection and try again")
            .setPositiveButton("Reload") { dialogInterface, _ ->
                dialogInterface.dismiss()
                checkInternetConnection()
            }
            .create()
            .show()
    }

    companion object {
        const val INFO_TITLE_TAG = "InfoTitleTag"
        const val INFO_TEXT_TAG = "InfoTextTag"
    }

}
