package com.yaylo.mornhouse.view.info

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import com.yaylo.mornhouse.R
import com.yaylo.mornhouse.databinding.FragmentNumberInfoBinding
import com.yaylo.mornhouse.view.search.SearchNumberFactsFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NumberInfoFragment : Fragment() {
    private var _binding: FragmentNumberInfoBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNumberInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
            findNavController().navigate(R.id.action_numberInfoFragment_to_searchNumberFactsFragment)
        }
        val title = arguments?.getString(SearchNumberFactsFragment.INFO_TITLE_TAG) ?: ""
        val text = arguments?.getString(SearchNumberFactsFragment.INFO_TEXT_TAG) ?: ""
        setInfo(title, text)
    }

    private fun setInfo(title: String, text: String) {
        if (title.isEmpty()) {
            binding.titleText.text = "Sorry, cannot find any facts"
        } else {
            binding.titleText.text = title
            binding.descriptionText.text = text
        }
    }
}
