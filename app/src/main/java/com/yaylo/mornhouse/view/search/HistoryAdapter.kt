package com.yaylo.mornhouse.view.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yaylo.mornhouse.R
import com.yaylo.mornhouse.data.local.entity.NumberInfoEntity

class HistoryAdapter(val onItemClick: (info: NumberInfoEntity) -> Unit) :
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {
    private var listNumbers: List<NumberInfoEntity> = mutableListOf()

    class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val text: TextView
        private val title: TextView

        init {
            text = itemView.findViewById(R.id.text)
            title = itemView.findViewById(R.id.title)
        }

        fun bind(info: NumberInfoEntity) {
            title.text = info.title
            text.text = info.text
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.number_info_item, parent, false)
        return HistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.bind(listNumbers[position])
        holder.itemView.setOnClickListener {
            onItemClick(listNumbers[position])
        }
    }

    override fun getItemCount() = listNumbers.size

    fun setItems(newList: List<NumberInfoEntity>) {
        listNumbers = newList
        notifyDataSetChanged()
    }
}