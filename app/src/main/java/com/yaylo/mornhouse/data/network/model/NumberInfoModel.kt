package com.yaylo.mornhouse.data.network.model

import com.google.gson.annotations.SerializedName
import com.yaylo.mornhouse.data.local.entity.NumberInfoEntity

data class NumberInfoModel (
    @SerializedName("text")
    val text: String? = null,
    @SerializedName("found")
    val found: Boolean = false,
    @SerializedName("number")
    val number: Long? = null,
    @SerializedName("type")
    val type: String? = null
)
