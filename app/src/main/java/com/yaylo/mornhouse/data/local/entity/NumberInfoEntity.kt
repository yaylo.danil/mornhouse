package com.yaylo.mornhouse.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yaylo.mornhouse.data.network.model.NumberInfoModel

@Entity(tableName = "info")
data class NumberInfoEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "text") val text: String
) {
    companion object {
        fun fromNetworkModel(info: NumberInfoModel) =
            NumberInfoEntity(
                title = info.number?.toString() ?: "",
                text = info.text ?: ""
            )
    }
}