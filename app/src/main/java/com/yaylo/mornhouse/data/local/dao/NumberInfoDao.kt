package com.yaylo.mornhouse.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.yaylo.mornhouse.data.local.entity.NumberInfoEntity

@Dao
interface NumberInfoDao {
    @Query("SELECT * FROM info")
    fun getAll(): List<NumberInfoEntity>?

    @Insert
    fun insert(users: NumberInfoEntity)

}
