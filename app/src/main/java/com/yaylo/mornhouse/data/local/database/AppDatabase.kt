package com.yaylo.mornhouse.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yaylo.mornhouse.data.local.dao.NumberInfoDao
import com.yaylo.mornhouse.data.local.entity.NumberInfoEntity

@Database(entities = [NumberInfoEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun numberInfoDao(): NumberInfoDao

    companion object {
        fun getDB(context: Context) =
            Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "storage.db")
                .build()
    }
}