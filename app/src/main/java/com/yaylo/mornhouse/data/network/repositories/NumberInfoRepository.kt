package com.yaylo.mornhouse.data.network.repositories

import com.yaylo.mornhouse.data.network.api.NumberApi
import javax.inject.Inject

class NumberInfoRepository @Inject constructor(private val numberApi: NumberApi) {
    suspend fun getInfoForNumber(number: Long) = numberApi.getInfoForNumber(number)
    suspend fun getInfoForRandomNumber() = numberApi.getInfoForRandomNumber()
}
