package com.yaylo.mornhouse.data.network.api

import com.yaylo.mornhouse.data.network.model.NumberInfoModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NumberApi {
    @GET("{number}?json")
    suspend fun getInfoForNumber(@Path("number") number: Long): Response<NumberInfoModel>

    @GET("random/math?json")
    suspend fun getInfoForRandomNumber(): Response<NumberInfoModel>
}
