package com.yaylo.mornhouse.di

import android.content.Context
import com.yaylo.mornhouse.data.local.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DbModule {
    @Singleton
    @Provides
    fun provideDb(@ApplicationContext context: Context) = AppDatabase.getDB(context)

    @Singleton
    @Provides
    fun provideNumberInfoDao(db: AppDatabase) = db.numberInfoDao()
}
