package com.yaylo.mornhouse.di

import com.yaylo.mornhouse.data.network.api.ApiFactory
import com.yaylo.mornhouse.data.network.api.NumberApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class ApiModule {
    @Singleton
    @Provides
    fun provideNumberApi(): NumberApi = ApiFactory.numberApi
}
