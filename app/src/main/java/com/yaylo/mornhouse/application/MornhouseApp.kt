package com.yaylo.mornhouse.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MornhouseApp: Application()
